** Costruzione di DFA **

Consideriamo l'alfabeto {a,b}. 
Realizzare dei DFA che riconoscono i seguenti linguaggi:
1 - stringhe con un numero dispari di a
2 - stringhe con un numero dispari di b e pari di a
3 - stringhe con un numero in cui le a e le b risultano essere entrambe 
in numero pari, oppure entrambe in numero dispari 
4 - stringhe con un numero pari di a ed almeno 3 b
5 - stringhe che terminano con ab
6 - stringhe che contengono la sottostringa aa
7 - stringhe che non contengono la sottostringa aa
8 - stringhe che contengono la sottostringa aaa o
la sottostringa aba (contengono almeno una delle due)

9 - Realizzare un DFA che riconosca il seguente linguaggio su alfabeto {0,1}:
stringhe che interpretate come numero binario risultano un multiplo di 5 

10 - Sempre considerando alfabeto {0,1}, realizzare un DFA che controlla la
correttezza delle somme binarie:
data la stringa: a_0 b_0 c_0 a_1 b_1 c_1 ... a_n b_n c_n controlla 
se a_n...a_1 a_0 + b_n...b_1 b_0 = c_n...c_1 c_0
(cioè a+b=c con a,b,c numeri binari con stessa lunghezza) 

** Costruzione di NFA **

11 Dato l'alfabeto {a,b} realizzare un NFA che terminano con ab

12 Dato l'alfabeto {a,b} realizzare un NFA che riconosce le stringhe che 
contengono aaa oppure aba

13 Realizzare un NFA che riconosce le stringhe sull'alfabeto {0,1,2,3} 
in cui l'ultima cifra appare almeno una volta in precedenza

14 Realizzare un NFA che riconosce le stringhe sull'alfabeto {0,1,2,3} 
in cui l'ultima cifra NON appare in precedenza

** Trasformazione da NFA a DFA **

15 Dato l'alfabeto {a,b} si consideri l'NFA fatto all'esercizio 12, che riconosce le stringhe che contengono aaa oppure aba. Trasformarlo in DFA.

** Costruzione di \epsilon-NFA **

16 Realizzare un epsilon-NFA che riconosce le stringhe sull'alfabeto {0,1,2,3} 
in cui l'ultima cifra NON appare in precedenza (lo avevamo fatto all'esercizio 14 con un NFA senza poter disporre delle transizioni epsilon).

Per casa: 

1 Valutare se e' possibile realizzare un DFA che controlli la  
correttezza delle somme binarie come nell'esercizio 9, che riceva 
pero' l'input nel formato a_n b_n c_n...a_0 b_0 c_0 

2 Valutare se e' possibile realizzare un DFA che controlli la  
correttezza delle somme binarie come nell'esercizio 9, che riceva 
pero' l'input nel formato a_0...a_n b_0...b_n c_0...c_n
